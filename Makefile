install:
	sudo nixos-rebuild switch --flake '.#traal'

update:
	sudo nix flake update && \
	sudo nixos-rebuild switch --upgrade --flake '.#traal'
	
clean:
	sudo nix-collect-garbage -d --delete-older-than 5d
	nix store optimise
