{
  lib,
  pkgs,
  ...
}: {
  networking.hostName = "traal"; # Define your hostname.

  nixpkgs = {
    hostPlatform = "x86_64-linux";
    config.allowUnfree = true;
  };

  services.fwupd.enable = true;
  system.stateVersion = "unstable";

  hardware = {
    enableRedistributableFirmware = true; # maybe obsolete
    enableAllFirmware = true;
    cpu.intel.updateMicrocode = true;
    opengl = {
      enable = true;
      extraPackages = with pkgs; [
        intel-media-driver # LIBVA_DRIVER_NAME=iHD
        # ^ this is actually already enabled by the nixos-hardware pkg
        intel-compute-runtime
      ];
    };
  };
  environment.sessionVariables.LIBVA_DRIVER_NAME = "iHD";

  # Bootloader.
  boot = {
    loader = {
      timeout = 1;
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    kernelPackages = pkgs.linuxPackages_xanmod_latest;

    initrd.availableKernelModules = ["xhci_pci" "thunderbolt" "nvme" "usb_storage" "sd_mod"];
    initrd.kernelModules = [];
    kernelModules = ["kvm-intel"];
    extraModulePackages = [];
  };

  services.tlp = {
    settings = {
      "SOUND_POWER_SAVE_ON_AC" = 0;
      # "SOUND_POWER_SAVE_ON_BAT" = 0;
      "SOUND_POWER_SAVE_CONTROLLER" = "Y";
      "USB_AUTOSUSPEND" = 0;
    };
    enable = true;
  };
  services.thermald.enable = true;

  services.logind = {
    lidSwitch = "hibernate";
    lidSwitchDocked = "hibernate";
    lidSwitchExternalPower = "hibernate";
  };

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/8e178eec-abf9-4b6d-b148-8eae42c0ac81";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/B912-328D";
    fsType = "vfat";
  };

  swapDevices = [
    {device = "/dev/disk/by-uuid/00ab6313-7001-41e9-9e2b-cc5788f30bdf";}
  ];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  services = {
    udev.extraRules = ''
      ACTION=="add", SUBSYSTEM=="thunderbolt", ATTR{authorized}=="0", ATTR{authorized}="1"
    '';
  };
  services.hardware.bolt.enable = true;
}
