{pkgs, ...}: {
  programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;
  environment.variables = {
    "PKG_CONFIG_PATH" = "${pkgs.openssl.dev}/lib/pkgconfig";
  };
  environment.systemPackages = with pkgs; [
    neovim
    wget
    git
    tmux
    gnumake
    xdg-utils
    rustup
    go
    helix
    nil
    ripgrep
    fd
    util-linux
    killall
    usbutils
    gping
    lazygit
    clang
    alejandra
    pkg-config
    openssl.dev
    fzf
    ranger
    zip
    p7zip
    ltex-ls
    lsd
    unzip
    starship
    jq
    unzip
    newsboat
    ffmpeg
    curlFull
    yt-dlp
  ];
}
