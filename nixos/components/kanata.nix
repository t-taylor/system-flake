{...}: {
  services.kanata = {
    enable = true;
    keyboards.laptop = {
      devices = ["/dev/input/by-path/platform-i8042-serio-0-event-kbd"];
      config = builtins.readFile ../../config/kanata/kanata.kbd;
      extraDefCfg = "process-unmapped-keys yes";
    };
  };
}
