{pkgs, ...}: {
  systemd.user.services = {
    mpd = {
      after = ["network.target" "sound.target"];
      description = "Music Player Daemon (User)";
      wantedBy = ["default.target"];
      script = "${pkgs.mpd}/bin/mpd --no-daemon --stderr";
    };
    easyeffects = {
      after = ["graphical.target"];
      description = "Easy effects service";
      wantedBy = ["default.target"];
      serviceConfig.Restart = "always";
      serviceConfig.RestartSec = 2;
      script = "${pkgs.easyeffects}/bin/easyeffects --gapplication-service";
    };
  };
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };
  environment.etc = {
    "pipewire/pipewire.conf.d/92-low-latency.conf".text = ''
      context.properties = {
        default.clock.rate = 48000
        default.clock.quantum = 128
        default.clock.min-quantum = 128
        default.clock.max-quantum = 128
      }
    '';
    "wireplumber/main.lua.d/51-fix-crackle.lua".text = ''
      rule = {
        matches = {
          {
            { "node.name", "matches", "alsa_output.*" },
          },
        },
        apply_properties = {
          [ "session.suspend-timeout-seconds"] = 0,
          [ "audio.format" ] = "S16LE",
          ["audio.rate"] = "96000",
          ["api.alsa.period-size"] = 2,
        },
      }
    '';
  };
  environment.systemPackages = with pkgs; [
    mpd
    vimpc
    beetsPackages.beets-minimal
    mpc-cli
    pavucontrol
    qpwgraph
    easyeffects
    gnome.adwaita-icon-theme # for easy effects icons
    rhythmbox
  ];
}
