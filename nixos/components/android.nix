{pkgs, ...}: {
  # for auto android sync
  services.gvfs.enable = true;

  programs.adb.enable = true;
  users.users.tom.extraGroups = ["adbusers"];

  environment.systemPackages = with pkgs; [
    jmtpfs
    libsForQt5.kdeconnect-kde
  ];
}
