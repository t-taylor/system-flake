{pkgs, ...}: {
  fonts = {
    enableDefaultPackages = true;
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji

      (nerdfonts.override {fonts = ["Iosevka"];})
    ];

    fontconfig = {
      defaultFonts = {
        monospace = ["Iosevka Nerd Font Mono"];
      };
    };
  };

  nixpkgs.overlays = [
    (final: prev: {qutebrowser = prev.qutebrowser.override {enableWideVine = true;};})
  ];
  programs.gnupg.agent = {
    enable = true;
    pinentryFlavor = "qt";
  };

  programs.dconf.enable = true;
  services.xserver.displayManager.defaultSession = "plasmawayland";

  environment.sessionVariables.NIXOS_OZONE_WL = "1";

  programs.hyprland = {
    enable = true;
  };
  xdg = {
    portal = {
      enable = true;
      extraPortals = [pkgs.xdg-desktop-portal-gtk];
    };
    mime = {
      enable = true;
      defaultApplications = {
        "text/html" = "org.qutebrowser.qutebrowser.desktop";
        "x-scheme-handler/http" = "org.qutebrowser.qutebrowser.desktop";
        "x-scheme-handler/https" = "org.qutebrowser.qutebrowser.desktop";
        "x-scheme-handler/about" = "org.qutebrowser.qutebrowser.desktop";
        "x-scheme-handler/unknown" = "org.qutebrowser.qutebrowser.desktop";
      };
    };
  };

  services.greetd = {
    enable = true;
    vt = 2;
    settings = {
      default_session.command = ''
             ${pkgs.greetd.tuigreet}/bin/tuigreet \
        --time \
        --asterisks \
        --user-menu \
         --remember \
        --cmd Hyprland
      '';
    };
  };

  environment.etc."greetd/environments".text = ''
    Hyprland
  '';

  security.polkit.enable = true;

  # # correctly order pam_fprintd.so and pam_unix.so so password and fignerprint works
  # security.pam.services.swaylock.text = ''
  #   # Account management.
  #   account required pam_unix.so

  #   # Authentication management.
  #   auth sufficient pam_unix.so nullok likeauth try_first_pass
  #   auth sufficient ${pkgs.fprintd}/lib/security/pam_fprintd.so
  #   auth required pam_deny.so

  #   # Password management.
  #   password sufficient pam_unix.so nullok sha512

  #   # Session management.
  #   session required pam_env.so conffile=/etc/pam/environment readenv=0
  #   session required pam_unix.so
  # '';

  environment.systemPackages = with pkgs; [
    swaybg
    swayidle
    batsignal
    brightnessctl
    wl-clipboard
    pass-wayland
    imv
    waybar
    mako
    libnotify #mako dep
    qutebrowser
    hunspell
    hunspellDicts.en_GB-large
    foot
    vlc
    rofi-wayland
    tessen
    swaylock
    pinentry-qt
    zathura
    libsForQt5.qtstyleplugin-kvantum
    adwaita-qt
    gnome.adwaita-icon-theme
    librewolf
    libreoffice-qt
    qbittorrent
    thunderbird-bin
    libsForQt5.polkit-kde-agent
    # localsend
    pcmanfm
    libsForQt5.okular
    libsForQt5.elisa

    grip
    slurp
  ];
}
