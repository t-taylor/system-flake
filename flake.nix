{
  description = "system setup";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
  };

  outputs = inputs: let
    system = "x86_64-linux";
  in {
    nixosConfigurations = {
      traal = inputs.nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = inputs;
        modules = [
          ./nix/settings.nix
          ./nixos/traal/hardware.nix
          ./nixos/components/kanata.nix
          ./nixos/components/system.nix
          ./nixos/networking/shared.nix
          ./nixos/networking/laptop.nix
          ./nixos/components/bluetooth.nix
          ./nixos/components/terminal.nix
          ./nixos/components/desktop-env.nix
          ./nixos/components/audio.nix
          ./nixos/components/slippi.nix
          ./nixos/components/android.nix
          # ./nixos/components/emu.nix
          ./nixos/components/games.nix
          inputs.nixos-hardware.nixosModules.framework-13th-gen-intel
          # home-manager.nixosModules.home-manager
          # {
          #   home-manager = {
          #     useUserPackages = true;
          #     useGlobalPkgs = true;
          #     users.<your_user_name> = ./home-manager/home.nix;
          #   };
          # }
        ];
      };
    };
  };
}
